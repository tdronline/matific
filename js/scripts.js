$(document).ready(function () {
    $.ajax({
        type: 'GET',
        url: 'https://ljifg6p8cd.execute-api.us-east-1.amazonaws.com/production/matific-test-gallery-activities',
        data: {get_param: 'value'},
        dataType: 'json',
        success: function (data) {
            $.each(data, function (index, element) {
                $('#episodes').append(
                    "<div class='slide'><div class='content'>" +
                    "<div class='image-wrap'><a data-episode='" + element.episode_url + "'><img src='" + element.thumbnail_url + "' alt='" + element.title + "'/></div></a>" +
                    "<h3>" + element.title + "<span class='more'></span></h3>" +
                    "<div class='popup'><a href=''>Open in Full Screen</a><a href=''>About the Activity</a><a href=''>Tips and Guideline</a></div></div></div>");
            });
            initSlider();
        }
    });

    $(document).on('click', '#episodes a', function (e) {
        e.preventDefault();
        var url = $(this).data('episode');
        $('#episode_frame iframe').attr('src', url);
        const fancybox = new Fancybox([{
            src: "#episode_frame",
            type: "inline",
        },]);

        fancybox.on("closing", (fancybox, slide) => {
            $('#episode_frame iframe').attr('src', '#');
        });
    });

    $(document).on('click', '#episodes .more', function (e) {
        $(this).closest('.content').find('.popup').toggleClass('active');
    });


});

// Sticky Start button
$(window).scroll(function (e) {
    var off = $(this).scrollTop();
    var h = $('header');
    var hoff = h.outerHeight();
    if (off > hoff) {
        h.addClass('sticky');
    } else {
        h.removeClass('sticky');
    }
});

function initSlider() {
    $('#episodes').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        touchThreshold: 100,
        arrows: false,
        centerMode: true,
        draggable: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
}